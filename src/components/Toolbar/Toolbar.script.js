import ConnectionService from "@/services/connection.service.js";
const data = () => {
  return {
    online: ConnectionService.onlineStatus,
    connectionLogs: ConnectionService.connectionLogs,
    menuItems: []
  };
};
const methods = {
  clearEntries(value) {
    return ConnectionService.clearEntries(value);
  }
};
const mounted = function() {
  const _this = this;
  browser.runtime.sendMessage({});
  this.menuItems = [
    {
      label: "Keep only last 30 days",
      action: () => _this.clearEntries(30)
    },
    {
      label: "Clear all entries",
      action: () => _this.clearEntries()
    }
  ];
};
const computed = {
  title() {
    return browser.i18n.getMessage("extName");
  }
};
const component = {
  name: "Toolbar",
  data,
  mounted,
  computed,
  methods
};

export default component;
