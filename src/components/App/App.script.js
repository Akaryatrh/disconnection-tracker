import ConnectionService from "@/services/connection.service.js";
import Toolbar from "@/components/Toolbar/Toolbar.vue";

const components = {
  Toolbar
};
const beforeCreate = () => {
  ConnectionService.initConnectionLogs();
  ConnectionService.initConnectionStatusListeners();
};
const component = {
  beforeCreate,
  components
};

export default component;
