import ConnectionService from "@/services/connection.service.js";

async function init() {
  await ConnectionService.initConnectionLogs();
  ConnectionService.initConnectionStatusListeners();
}

init();
