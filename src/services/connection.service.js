import _isInteger from "lodash/isInteger";
import { parseISO, differenceInDays } from "date-fns";
class Service {
  connectionLogs = [];
  onlineStatus = {
    value: navigator.onLine
  };

  _getStoredConnectionLogs() {
    return new Promise(resolve => {
      chrome.storage.local.get(["connectionLogs"], data => {
        resolve(
          data && data.connectionLogs && data.connectionLogs.length
            ? data.connectionLogs
            : []
        );
      });
    });
  }

  initConnectionStatusListeners() {
    window.addEventListener("online", () => this.addNewConnectionLog(true));
    window.addEventListener("offline", () => this.addNewConnectionLog(false));
  }
  async initConnectionLogs() {
    const logs = await this._getStoredConnectionLogs();
    console.log("logs", logs);
    logs.forEach(log => this.connectionLogs.push(log));
  }

  addNewConnectionLog(online) {
    this.setOnlineStatus(online);
    const date = new Date().toISOString();
    const entry = {
      online,
      date
    };
    this.connectionLogs.push(entry);
    chrome.storage.local.set({ connectionLogs: this.connectionLogs });
  }

  getConnectionLogs() {
    return this.connectionLogs;
  }

  getOnlineStatus() {
    return this.onlineStatus;
  }

  setOnlineStatus(status) {
    this.onlineStatus.value = status;
  }

  clearEntries(value) {
    // Remove days
    if (_isInteger(value)) {
      const filteredLogs = this.connectionLogs.filter(log => {
        const parsedDate = parseISO(log.date);
        return differenceInDays(new Date(), parsedDate) <= value;
      });

      this.connectionLogs.splice(0, this.connectionLogs.length);
      filteredLogs.forEach(log => this.connectionLogs.push(log));
    }

    // Remove specific entry
    if (typeof value === "string") {
      const index = this.connectionLogs.findIndex(log => log.date === value);

      this.connectionLogs.splice(index, 1);
    }

    // Remove all
    if (!value) {
      this.connectionLogs.splice(0, this.connectionLogs.length);
    }
    console.log("this.connectionLogs", this.connectionLogs);
    chrome.storage.local.set({ connectionLogs: this.connectionLogs });
  }
}

export default new Service();
