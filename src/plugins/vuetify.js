import Vue from "vue";
import Vuetify, { VToolbarTitle } from "vuetify/lib";
const components = {
  VToolbarTitle
};
Vue.use(Vuetify);

export default new Vuetify({
  components
});
