import { format, parseISO } from "date-fns";
import _groupBy from "lodash/groupBy";

import ConnectionService from "@/services/connection.service.js";

const data = () => {
  return {
    connectionLogs: ConnectionService.connectionLogs
  };
};

const computed = {
  logsDates() {
    return Object.keys(this.groupedByDayLogs).reverse();
  },

  groupedByDayLogs() {
    return _groupBy(this.connectionLogs, log => {
      const parsedDate = parseISO(log.date);
      return format(parsedDate, "yyyy-MM-dd");
    });
  }
};

const methods = {
  formatDate(date) {
    const parsedDate = parseISO(date);
    return format(parsedDate, "dd/MM/yyyy, HH:mm:ss");
  },

  getDate(date) {
    const todayFormatted = format(new Date(), "yyyy-MM-dd");
    if (todayFormatted === date) {
      return "Today";
    }

    const parsedDate = parseISO(date);
    return format(parsedDate, "EEEE, do 'of' LLLL yyyy");
  },

  sortLogs(date) {
    return this.groupedByDayLogs[date]
      .sort((a, b) => a.date.localeCompare(b.date))
      .reverse();
  },

  clearEntries(value) {
    return ConnectionService.clearEntries(value);
  }
};

const component = {
  data,
  computed,
  methods
};

export default component;
