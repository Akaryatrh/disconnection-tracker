module.exports = {
  // Temporary until https://github.com/adambullmer/vue-cli-plugin-browser-extension/pull/64 is merged
  configureWebpack: config => {
    if (process.env.NODE_ENV === "development") {
      config.optimization.splitChunks.cacheGroups.vendors.chunks = () => false;
      config.optimization.splitChunks.cacheGroups.common.chunks = () => false;
    }
  },
  pages: {
    popup: {
      template: "public/browser-extension.html",
      entry: "./src/popup/main.js",
      title: "Popup"
    }
  },
  pluginOptions: {
    browserExtension: {
      componentOptions: {
        background: {
          entry: "src/background.js"
        }
      }
    }
  },
  transpileDependencies: ["vuetify"]
};
